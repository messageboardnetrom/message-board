<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Categorii
 *
 * @ORM\Table(name="categorii")
 * @ORM\Entity
 */
class Categorii
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nume
     *
     * @ORM\Column(name="nume", type="string", length=255, nullable=false)
     */
    private $nume;

    /**
     * @var text $descriere
     *
     * @ORM\Column(name="descriere", type="text", nullable=true)
     */
    private $descriere;


}