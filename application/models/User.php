<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nume
     *
     * @ORM\Column(name="nume", type="string", length=255, nullable=false)
     */
    private $nume;

    /**
     * @var string $parola
     *
     * @ORM\Column(name="parola", type="string", length=255, nullable=false)
     */
    private $parola;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string $tip
     *
     * @ORM\Column(name="tip", type="string", length=255, nullable=false)
     */
    private $tip;

    /**
     * @var boolean $activ
     *
     * @ORM\Column(name="activ", type="boolean", nullable=true)
     */
    private $activ;


}