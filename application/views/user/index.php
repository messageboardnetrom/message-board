<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="form-box right">
				<h2>Login</h2>
				<form class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-2">
							<label for="username">Username</label>
						</div>
						<div class="col-sm-10">
							<input type="text" name="username" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
							<label for="password">Password</label>
						</div>
						<div class="col-sm-10">
							<input type="password" name="password" />
						</div>
					</div>
						<button type="submit" class="form-box-submit-button btn-lg">Login</button>
					
				</form>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-box">
				<h2>Register</h2>
				<form class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-2">
							<label for="username">Register</label>
						</div>
						<div class="col-sm-10">
							<input type="text" name="username" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
							<label for="password">Password</label>
						</div>
						<div class="col-sm-10">
							<input type="password" name="password" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-2">
							<label for="email">E-mail</label>
						</div>
						<div class="col-sm-10">
							<input type="text" name="email" />
						</div>
					</div>
						<button type="submit" class="form-box-submit-button btn-lg">Register</button>
					
				</form>
			</div>


		</div>

	</div>
</div>
